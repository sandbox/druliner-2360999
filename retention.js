/**
 *
 * @file
 * Modify retention field and display reminders if this node is subject to retention.
 */

(function ($) {
jQuery(document).ready(function($) {

  var retention_fieldset = $('div.form-item-revision-retention-field-und-0-value');
  var retention_field = $('#edit-revision-retention-field-und-0-value');
	
  var retention_confimation_message = Drupal.t('The page you are attempting to edit is subject to revision retention. \n\n');
    retention_confimation_message += Drupal.t('If this is a minor revision, click "OK" to continue saving your changes. \n\n');
    retention_confimation_message += Drupal.t('If the edits were in fact "significant"*\n');
    retention_confimation_message += Drupal.t('  1) Click "Cancel"\n  2) Click "Create new revision"\n  3) Provide a "Revision log message" \n  4) Click "Save" again.');
    retention_confimation_message += Drupal.t('\n\n* For more information on the types of changes requiring retention contact your Records Management office.\n');

  // == Move the retention field to the revisions tab ===================
    // copy the retention field set content.
    var retention_fieldset_content = retention_fieldset.html();

    // Delete retention fieldset from the original location.
    retention_fieldset.html('');

    // Rewrite it in the new location.
    $('fieldset#edit-revision-information div.fieldset-wrapper').append(retention_fieldset_content);

  // == Build new form submit handler ==============================
  $('form.node-form').submit(function(event){ return retention_handler()});

  // On submit see if the page is subject to retention and, if so, prompt user.
  function retention_handler() {
    if (retention_field.val() != ''){
    // Revisions checkbox checked.
    if($('input#edit-revision:checked').length > 0){
        if($('textarea#edit-log').val() == ''){
          var log_error = Drupal.t('If you create a new revision, you must provide a log message.');
          alert(log_error);
          return false;
        }
      } else {
        // The revision checkbox wasn't selected so display the confirmation.
        // Highlight revision box.
        revision_info_click();

        // Confirm this is a minor change, or allow them to cancel and add a revision.
         return confirm(retention_confimation_message);
      }

    }
  } // /retention_handler

  // == Redirect Error to new location ========== //
  if ($("div#console div.error em:contains('Retain revisions')").text() != ''){
     $('#edit-field-retain-revisions-for-und-0-value').css({ "border": "thin solid red",  });
     revision_info_click();
  }

  function revision_info_click(){
          $("strong:contains('Revision information')").click();
  }

});
}(jQuery));
