<?php
/**
 * @file
 * Administration page callbacks for the retention module.
 *
 * Creates the administration pages to control module settings.
 */
function retention_admin_settings() {
  // Get an array of node types with internal names as keys and
  // "friendly names" as values. E.g.,
  // array('page' => �Basic Page, 'article' => 'Articles')
  $types = node_type_get_types();
  foreach ($types as $node_type) {
    $options[$node_type->type] = $node_type->name;
  }

  $form['retention_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Admins may enable retention on the following node types.'),
    '#options' => $options,
    '#default_value' =>
    variable_get('retention_node_types', array('page')),
    '#description' =>
    t('A text field in the revision section of these node edit pages
    will denote if, and how long, to save revisions.'),
  );

  $form['#submit'][] = 'retention_admin_settings_submit';
  return system_settings_form($form);

}


/**
 * Process annotation settings submission.
 */
function retention_admin_settings_submit($form, $form_state) {

  // Loop through each content type
  foreach ($form_state['values']['retention_node_types'] as $key => $value) {
  
    $instance = field_info_instance('node', 'revision_retention_field', $key);
  
    if( $value && !$instance ){ // Content type is checked: Create instance if it doesn't exists
		  	  
	  $instance = array(
		'field_name' => 'revision_retention_field',
		'label' => 'Revision retention field',
		'entity_type' => 'node',
		'bundle' => $key,
	  
		'display'     => array(
		  'default' => array(
			'type' => 'hidden',
		  ),
		),
	  );
    
	  field_create_instance($instance);
		watchdog( 'Retention', 'Creating instance of retention field in: ' . $key );

	  
	}else if (!$value && $instance ) { // Content type is NOT checked: Delete instance if it exists
	
		field_delete_instance($instance, false);
		watchdog( 'Retention', 'Delete instance of retention field in: %key', array('%key' => $key) );
  
	}
  
  } // End foreach loop.
}
