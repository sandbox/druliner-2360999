CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
Organizations that engage in Records Management typically require that
documents created by the organization be retained for a certain period
of time and then be destroyed. This module assists in records retention
by enabling site administrators to mark specific nodes for retention and
then generating a report showing which revisions of that node have reached
the end of their retention period.


REQUIREMENTS
------------
This module requires the (core) node module and that Javascript be enabled 
in the users browser.


INSTALLATION
------------
Install as you would normally install a contributed drupal module. 
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------
 * Configure which content types are available for retention in 
 admin/config/content/retention
 * Specify a retention period for a specific node in the revisions section of 
 the edit screen for that node.
 * View revisions that have exceeded their retention period at 
 admin/reports/retention


TROUBLESHOOTING & FAQ
---------------------
You may have several revisions of a node that have exceeded their retention
period. The report only shows a delete link for the oldest one. If you want
to see all revisions on a node, click the "Node Title" link in the report.

Q: Why are old revisions not being flagged in my report?
A: Current revisions that are X months old aren't flagged. But old revisions
that have a newer revisions that is X months old are flagged. 


MAINTAINERS
-----------
Current maintainers:
 * Dan Druliner (University of Washington) - https://www.drupal.org/user/668834
